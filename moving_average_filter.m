% moving_average_filter function eliminates any low frequency data 
function filtered_data = moving_average_filter(unfiltered_data)
prompt 	= 'what is the strength of moving average?'
n 		= input(prompt)

% Loop through data
for t = 1: length(unfiltered_data)
    xn = t - n;
    if t == 1 
        filtered_data(1) = unfiltered_data(1);     
    else 
          if xn < 1 
                xn = 1;
          end 
                filtered_data(t) = filtered_data(t-1) + (1/n)*((unfiltered_data(t) - unfiltered_data(xn)));
    end 
end

% 
filtered_data = filtered_data;